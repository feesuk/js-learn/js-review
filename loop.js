/**
 * Soal 1
 input: [1, 2, 3, empat, 5, enam]; 
 outputnya: [number, number, number.string, number, string];
 */
let someNumber = [1, 2, 3, "empat", 5, "enam"];

function dataTypeCheck(input) {
  let output = [];

  for (let index = 0; index < input.length; index++) {
    output.push(typeof input[index]);
  }
  return output;
}

console.log(dataTypeCheck(someNumber));
